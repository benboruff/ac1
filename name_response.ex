defmodule NameResponse do
  def start do
    input = IO.gets(~s{Enter your name:})

    case String.downcase(String.trim(input)) do
      "ben" -> ~s{Wow, that's my favorite name! The person who programmed me is named Ben}
      _ -> ~s{Hello, #{String.trim(input)}. Nice to meet you!}
    end
  end
end
